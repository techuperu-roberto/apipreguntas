package com.TechU.APISUCAMEC.Controllers;

import com.TechU.APISUCAMEC.models.questions;
import com.TechU.APISUCAMEC.services.QuestionService;
import com.TechU.APISUCAMEC.services.correctAnswerOnly;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

@RestController
public class QuestionsController {

    @Autowired
    private QuestionService questionService;

    @GetMapping(value = "/v1/questions")
    public String getQuestionsAPI() {
        String urlAPI = "https://questionsucamec.s3-sa-east-1.amazonaws.com/questionsucamec.json";
        String questions = "";

        try {
            URL url = new URL(urlAPI);
            HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("User-Agente", "Myapp");
            conexion.setRequestProperty("Content.Type", "application/json; charset=utf-8");
            InputStreamReader entrada = new InputStreamReader(conexion.getInputStream());
            int respuesta = conexion.getResponseCode();
            System.out.println(new StringBuilder().append("Respuesta Servicio: ").append(respuesta));
            if (respuesta == HttpURLConnection.HTTP_OK) {
                BufferedReader lector = new BufferedReader(entrada);
                String lineaLeida;
                StringBuffer resultado = new StringBuffer();
                while ((lineaLeida = lector.readLine()) != null) {
                    resultado.append(lineaLeida);
                }
                lector.close();
                questions = resultado.toString();
            }
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return questions;

    }

    @Autowired
    ResourceLoader cargador;

    @GetMapping(value = "/v2/questions")
    public String getquestionsDeJson() {
        String nombreFichero = "classpath:static/questions.json";
        Resource recursoJson = cargador.getResource(nombreFichero);
        String questions = "";
        try {
            InputStream streamDatos = recursoJson.getInputStream();
            byte[] bytesDocumento = streamDatos.readAllBytes();
            questions = new String(bytesDocumento, StandardCharsets.UTF_8);
            streamDatos.close();
            System.out.println((questions));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return questions;
    }

    @GetMapping(value = "/v3/questions")
    public JSONArray getquestionDeJsonObject() {
        JSONParser parser = new JSONParser();
        try {
            JSONArray jsonArray = (JSONArray) parser.parse(new FileReader("C:/Users/resta/TechU/APISUCAMEC/src/main/resources/static/questions.json"));
            System.out.println(jsonArray.get(1));
            System.out.println(jsonArray.size());
            System.out.println(jsonArray);
            Gson json = new Gson();
            questions pregunta = new questions();
            pregunta = json.fromJson(String.valueOf(jsonArray.get(1)), questions.class);
            //System.out.println(pregunta.getId());
            return (jsonArray);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping(value = "/v4/questions/{id}")
    public ResponseEntity getQuestionsId(@PathVariable("id") Integer id) {
        try {
            Gson json = new Gson();
            questions pregunta = new questions();
            //pregunta = json.fromJson(String.valueOf(getquestionDeJsonObject().get(id - 1)), questions.class);
            pregunta = json.fromJson(String.valueOf(questionService.getPreguntasId(id)), questions.class);
            if (pregunta == null) {
                return new ResponseEntity<>("Pregunta no encontrada.", HttpStatus.NOT_FOUND);
            }
            return ResponseEntity.ok(pregunta);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return new ResponseEntity<>("Numero de pregunta no existe", HttpStatus.BAD_REQUEST);
        } catch (NumberFormatException numberFormatException) {
            numberFormatException.printStackTrace();
            return new ResponseEntity<>("ID pregunta debe ser entero", HttpStatus.BAD_REQUEST);
        }
        return null;
    }

    @GetMapping(value = "/v4/questions")
    public JSONArray getQuestionsDeService() {
        try {
            JSONArray arreglo = new JSONArray();
            arreglo = questionService.getPreguntas();
            //System.out.println(arreglo);
            return arreglo;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping(value = "/v4/questions")
    public ResponseEntity<String> addQuestion(@RequestBody questions newquestions) {
        Gson json = new Gson();
        questions pregunta = new questions();
        try {
            int idPreguntaNueva = newquestions.getId();
            pregunta = json.fromJson(String.valueOf(questionService.getPreguntasId(idPreguntaNueva)), questions.class);
            if (pregunta == null) {
                questionService.addQuestion(newquestions);
                return new ResponseEntity<>("Pregunta agregada correctamente", HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>("Pregunta ya definida", HttpStatus.ALREADY_REPORTED);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Pregunta ya definida", HttpStatus.ALREADY_REPORTED);
        }
    }

    @PutMapping(value = "/v4/questions/{id}")
    public ResponseEntity updateQuestion(@PathVariable int id, @RequestBody questions questionUpdate) {
        questions pregunta = new questions();
        Gson json = new Gson();
        try {
            int idPreguntaUpdate = questionUpdate.getId();
            pregunta = json.fromJson(String.valueOf(questionService.getPreguntasId(idPreguntaUpdate)), questions.class);
            if (pregunta == null) {
                return new ResponseEntity<>("Pregunta no existe en base", HttpStatus.NOT_FOUND);
            } else {
                questionService.updateQuestion(id, questionUpdate);
                return new ResponseEntity<>("Pregunta Actualizada", HttpStatus.ACCEPTED);
            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return new ResponseEntity("Error en cuerpo del JSON", HttpStatus.BAD_REQUEST);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return new ResponseEntity("Error en id de la pregunta", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/v4/questions/{id}")
    public ResponseEntity delQuestion(@PathVariable int id) {
        questions pregunta = new questions();
        Gson json = new Gson();
        pregunta = json.fromJson(String.valueOf(questionService.getPreguntasId(id)), questions.class);
        if (pregunta == null) {
            return new ResponseEntity("Pregunta no encontrada", HttpStatus.NOT_FOUND);
        } else {
            questionService.deleteQuestion(id);
            return new ResponseEntity("Pregunta elimiinada", HttpStatus.OK);
        }
    }

    @PatchMapping(value = "/v4/questions/{id}")
    public ResponseEntity PatchCorrectAnswer(@PathVariable int id, @RequestBody correctAnswerOnly correctAnswerOnly) {
        questions pregunta = new questions();
        Gson json = new Gson();
        try {
            pregunta = json.fromJson(String.valueOf(questionService.getPreguntasId(id)), questions.class);
            if (pregunta == null) {
                return new ResponseEntity<>("Pregunta no existe en base", HttpStatus.NOT_FOUND);
            } else {
                pregunta.setCorrectAnswer(correctAnswerOnly.getCorrectAnswer());
                questionService.updateQuestion(id, pregunta);
                return new ResponseEntity(pregunta, HttpStatus.OK);
            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping(value = "v4/questions/{id}/answers")
    public ResponseEntity getQuestionIdAnsers(@PathVariable int id) {
        questions pregunta = new questions();
        Gson json = new Gson();
        try {
            pregunta = json.fromJson(String.valueOf(questionService.getPreguntasId(id)), questions.class);
            if (pregunta == null) {
                return new ResponseEntity<>("Pregunta no existe en base", HttpStatus.NOT_FOUND);
            } else {
                return ResponseEntity.ok(pregunta.getAnswers());
            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        return null;
    }
}