package com.TechU.APISUCAMEC.services;

import com.TechU.APISUCAMEC.models.questions;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

@Component
public class QuestionService {
    JSONArray preguntas = new JSONArray();

    public QuestionService(){
        JSONParser parser = new JSONParser();
        try {
            JSONArray jsonArray = (JSONArray) parser.parse(new FileReader("C:/Users/resta/TechU/APISUCAMEC/src/main/resources/static/questions.json"));
            preguntas = jsonArray;
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public JSONArray getPreguntas() {
        return preguntas;
    }

    public JSONObject getPreguntasId(int id) throws IndexOutOfBoundsException{
        if((preguntas.size()>=id)&&(id>0)){
            return (JSONObject) preguntas.get(id-1);
        }
        return null;
    }

    public void setPreguntas(JSONArray preguntas) {
        this.preguntas = preguntas;
    }
    public void addQuestion(questions newQuestion){
        preguntas.add(newQuestion);
    }

    public void updateQuestion(int index, questions newQuestion) throws IndexOutOfBoundsException{
        preguntas.set(index-1,newQuestion);
    }
    public void deleteQuestion(int index) throws IndexOutOfBoundsException{
        preguntas.remove(index-1);
    }
}
