package com.TechU.APISUCAMEC;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class QuestionsControllerTest {
    @Autowired
    private MockMvc mvc;

    @Test
    public void getQuestionsAPI() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/v1/questions"))
                .andExpect(status().isOk());
    }

    @Test
    public void getquestionsDeJson() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/v2/questions"))
                .andExpect(status().isOk());
    }

    @Test
    public void getquestionDeJsonObject() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/v3/questions"))
                .andExpect(status().isOk());
    }

    
    public void getQuestionsId() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/v4/questions/01")).
                andExpect(status().isOk());
    }

    @Test
    public void GetQuestionInexistente() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/v3/questions/11")).
                andExpect(status().isNotFound());
    }

    @Test
    public void getQuestionv4() throws Exception{
        mvc.perform(MockMvcRequestBuilders.get("/v4/questions")).
                andExpect(status().isOk());
    }
}
